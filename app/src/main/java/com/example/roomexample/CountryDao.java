package com.example.roomexample;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class CountryDao {

    @Insert
    public abstract void insertAll(List<Country> countries);
    @Query("SELECT * FROM Country")
    public abstract Flowable<List<Country>> selectAll();

    @Query("DELETE FROM Country")
    public abstract void removeAll();
}
