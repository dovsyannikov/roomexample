package com.example.roomexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    MyDataBase dataBase;
    CompositeDisposable disposables = new CompositeDisposable();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();

        generateMethod();


        printCountries();
    }

    private void generateMethod() {
        Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                List<Country> list = new ArrayList<>();
                list.add(new Country(1, "Ukraine", "Europe"));
                list.add(new Country(2, "OAO", "Asia"));

                dataBase.getCountryDao().removeAll();
                dataBase.getCountryDao().insertAll(list);

                return true;
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }



    private void printCountries(){
        disposables.add(dataBase.getCountryDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> countries) throws Exception {
                        for (Country country : countries) {
                            Log.d("data", country.name);
                        }
                    }
                }));



        }
    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }
}
